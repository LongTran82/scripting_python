#! /usr/bin/env python3
# -*- coding: utf-8

from bs4 import BeautifulSoup
import requests
import argparse
from urllib.parse import urlparse, urljoin


'''Ecrire une commande brklnk en Python 3 qui parcoure tous les liens contenus dans une page web et affiche les liens cassés.'''

def brklnk(url, depth):
    if depth >= 0:
        req = requests.get(url)
        if req.status_code >= 404:
            print(url + "  -->  Lien cassé")
        soup = BeautifulSoup(req.text, 'html.parser')
        # search all links in the url given
        for link in soup.find_all('a'):
            linkfound = link.get('href')
            # relative url will be switched in absolute url
            linkfound = urlparse(linkfound)
            if not linkfound.scheme:
                linkfound = urljoin(url, linkfound.path)
            brklnk(linkfound, depth-1)
    return

def main():
    parser = argparse.ArgumentParser(description='looking for broken links')
    parser.add_argument('url', help = "url of the site analysed", type= str)
    parser.add_argument('-d', '--depth', help = "depth of the link to be analysed", type=int, default = 1)
    args = parser.parse_args()
    brklnk(args.url, args.depth)    

if __name__ == "__main__":
        main()