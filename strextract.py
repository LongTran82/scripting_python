#!/usr/bin/env python3
#-*- coding:Utf-8
'''
Ecrire un programme Python 3 qui parcours récursivement tous les fichiers d’une hiérarchie et produit en sortie toutes les chaînes littérales (= les textes entourés par " ou ’) que contiennent ces fichiers.

Les chaînes seront affichées sous la forme où elles étaient dans le fichier, entourées des même symboles " ou ’.
'''

import os
import re
import argparse

def main():
    # create the parser to select the argument of the function directly in the command line
    parser = argparse.ArgumentParser(description='Print the string type data in the directory')
    parser.add_argument("directory", help="path to the directory from which the research is made", type=str)
    parser.add_argument('--all', '-a', help="Hidden files (those whose name begins with '.') are also included", action='store_true')
    parser.add_argument('--path', '-p', help='each line produced is preceded by the path to the associated file followed by a tabulation', action='store_true')
    parser.add_argument('--suffix', '-s', help="only search on files with suffix", type=str)
    args=parser.parse_args()
    strextract(args)

def strextract(args):
    #store search for "" or ''
    regex_compiled = re.compile(r"(\"|\').*?(\"|\')")
    #browse the files in the arg directory
    for root, _, filenames in os.walk(args.directory):
        for filename in filenames:
            if filename.startswith(".") and not args.all:
                continue
            if args.suffix and not filename.endswith(args.suffix):
                continue
            pathname = os.path.join(root, filename)
            try:
                with open(pathname) as file:
                    for line in file:
                        for match in regex_compiled.finditer(line):
                            if args.path:
                                print(pathname, match.group(0), sep="\t") 
                            else:
                                print(match.group(0))
            except:
                pass

if __name__ == "__main__":
    main()

